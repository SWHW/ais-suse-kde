# AIS-SUSE-KDE

Auto Install Script for openSUSE KDE.

**WHAT**
* Adds missing repositories.
* Installs my preferred software.
* Removes unwanted software.

#########################################

**Repositories add**
* Main Repository (NON-OSS)
* Packman Repository
* libdvdcss repository
* home:harish2704:obs-studio



**Package list:**
* Aegisub
* Audacious
* Blender
* Darktable
* Discord
* GIMP
* Gamemode
* Handbrake-gtk
* Inkscape
* KVM
* Krita
* Lutris
* MkvToolNix-gui
* OBS Studio
* QEMU
* Quassel-mono
* RAWTherapee (appimage)
* Olive
* Steam
* Telegram Desktop
* Texstudio
* Transmission-QT
* Wine-staging
* ffmpeg
* mpv Media Player
* neofetch
* unrar
* youtube-dl


**Uninstall list:**
* VLC
* Ktorrent
* KDE Games
* Konversation

